package nl.royalteazer.rtplanner;

import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Optional;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AttachmentController {

    Logger logger = LoggerFactory.getLogger(AttachmentController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Value("${rtplanner.attachment.dir}")
    private String attachmentDirectory;

    @GetMapping("/attachments/{songid}")
    public Collection<Attachment> getAttachments(@PathVariable("songid") Long songid) {
        logger.info("Attachments requested of song {}.", songid);

        List<Attachment> attachments = new ArrayList<>();

        // Iterate through all attachments.
        for (Attachment attachment : attachmentRepository.findAll()) {
            if (attachment.getSongid().equals(songid)) {
                attachments.add(attachment);
            }
        }

        return attachments;
    }


    @GetMapping(value = "/attachment/{attachmentid}")
    public ResponseEntity<byte[]> getAttachmet(@PathVariable("attachmentid") Long attachmentid) {
        logger.info("Requesting attachment {}.", attachmentid);

        Optional<Attachment> result = attachmentRepository.findById(attachmentid);
        if (result.isPresent()) {
            try {
                String filename = result.get().getAttachmentfilename();
                Path pathname = Paths.get(attachmentDirectory, filename);
        
                byte[] contents = Files.readAllBytes(pathname);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_PDF);
                headers.setContentDispositionFormData(filename, filename);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                return new ResponseEntity<>(contents, headers, HttpStatus.OK);
            } catch (IOException e) {
                throw new ResponseStatusException(HttpStatus.NO_CONTENT, "PDF not found");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Attachment not found");
        }
    }

    @DeleteMapping(value = "/attachment/{attachmentid}")
    public Map<String, String> deleteAttachment(
        @PathVariable("attachmentid") Long attachmentid, 
        @RequestHeader("token") String token) {

        logger.info("Deleting attachment {}.", attachmentid);

        // Look up user and check if key has expired.
        for (User user : userRepository.findAll()) {
            if ((user.getToken() != null) && (user.getToken().equals(token))) {
                if (user.getTokenexpirytime().isBefore(Instant.now())) {
                    logger.info("Login of user {} has expired.", user.getUsername());
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
                }

                Optional<Attachment> optional = attachmentRepository.findById(attachmentid);
                if (optional.isPresent()) {
                    Attachment attachment = optional.get();
                    Path pathname = Paths.get(attachmentDirectory, attachment.getAttachmentfilename());
                    if (pathname.toFile().delete()) {
                        attachmentRepository.delete(attachment);
                        Map<String, String> response = new HashMap<>();
                        response.put("result", "ok");
                        return response;        
                    } else {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "File not found");
                    }

                } else {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found");
                }
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
    }

    @PostMapping("/claim/attachments")
    public Map<String, String> claimAttachments(
        @RequestBody PostClaim claim,
        @RequestHeader("token") String token) {
        logger.info("Claiming attachments for son {}.", claim.getSongid());

        // Look up user and check if key has expired.
        for (User user : userRepository.findAll()) {
            if ((user.getToken() != null) && (user.getToken().equals(token))) {
                if (user.getTokenexpirytime().isBefore(Instant.now())) {
                    logger.info("Login of user {} has expired.", user.getUsername());
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
                }

                for (Long attachmentid : claim.getAttachmentids()) {
                    Optional<Attachment> optional = attachmentRepository.findById(attachmentid);
                    if (optional.isPresent()) {
                        Attachment attachment = optional.get();
                        attachment.setSongid(claim.getSongid());
                        attachmentRepository.save(attachment);
                    }
                }

                Map<String, String> response = new HashMap<>();
                response.put("result", "ok");
                return response;
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
    }

    @PostMapping("/attachment/{songid}")
    public Map<String, String> postAttachment(
            @PathVariable("songid") Long songid,
            @RequestParam("file") MultipartFile file,
            @RequestHeader("token") String token
        ) {

        logger.info("Posting attachment {}.", file.getOriginalFilename());
 
        // Look up user and check if key has expired.
        for (User user : userRepository.findAll()) {
            if ((user.getToken() != null) && (user.getToken().equals(token))) {
                if (user.getTokenexpirytime().isBefore(Instant.now())) {
                    logger.info("Login of user {} has expired.", user.getUsername());
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
                }

                Path pathname = Paths.get(attachmentDirectory, file.getOriginalFilename());
                try {
                    file.transferTo(pathname.toFile());
                } catch (IllegalStateException | IOException e) {
                    logger.warn("Error while writing file.");
                    e.printStackTrace();
                    throw new ResponseStatusException(HttpStatus.INSUFFICIENT_STORAGE, "Cannot write file.");
                }

                // Save to database.
                Attachment attachment = new Attachment();
                attachment.setSongid(songid);
                attachment.setComments("");
                attachment.setDate(LocalDate.now());
                attachment.setAttachmentfilename(file.getOriginalFilename());
                Attachment savedAttachment = attachmentRepository.save(attachment);

                Map<String, String> response = new HashMap<>();
                response.put("attachmentid", Long.toString(savedAttachment.getAttachmentid()));
                return response;
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
    }
}