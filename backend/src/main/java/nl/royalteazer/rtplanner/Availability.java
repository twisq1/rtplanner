package nl.royalteazer.rtplanner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;
import java.time.LocalTime;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Availability {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long availabilityid;

    private LocalDate date;
    private Long userid;
    private boolean available;
    private LocalTime fromtime;
    private LocalTime totime;
    private String comments;
}
