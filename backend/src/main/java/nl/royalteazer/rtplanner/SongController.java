package nl.royalteazer.rtplanner;

import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class SongController {

    static String SONGFILENAME_PREFIX = "song_";

    static Pattern reBody = Pattern.compile("<body>(.*)</body>");

    Logger logger = LoggerFactory.getLogger(SongController.class);

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Value("${rtplanner.attachment.dir}")
    private String attachmentDirectory;

    @Value("${rtplanner.chordpro.dir}")
    private String chordproDirectory;

    @Value("${rtplanner.chordpro.update}")
    private String updateScript;

    @Value("${rtplanner.chordpro.updatesongbook}")
    private String updateSongbookScript;

    @Value("${rtplanner.chordpro.songbook}")
    private String songbookFilepath;

    @GetMapping("/songs")
    public Collection<SongWithoutChords> songs() {
        logger.info("Songlist requested");
        List<SongWithoutChords> songs = new ArrayList<>();

        // Get list of songs.
        for (Song song : songRepository.findAll()) {
            SongWithoutChords record = new SongWithoutChords();
            record.setSongid(song.getSongid());
            record.setSongname(song.getSongname());
            record.setDate(song.getDate());
            record.setComments(song.getComments());
            songs.add(record);
        }

        return songs;
    }

    @GetMapping(value = "/song/crd/{songid}")
    public Song getSongAsCrd(@PathVariable("songid") Long songid) {
        logger.info("Requesting song number {} as CRD.", songid);

        Optional<Song> result = songRepository.findById(songid);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Song not found");
        }
    }

    @GetMapping(value = "/song/pdf/{songid}")
    public ResponseEntity<byte[]> getSongAsPdf(@PathVariable("songid") Long songid) {
        logger.info("Requesting song number {} as PDF.", songid);

        Optional<Song> result = songRepository.findById(songid);
        if (result.isPresent()) {
            try {
                String basename = SONGFILENAME_PREFIX + result.get().getSongid();
                String internalFilename = basename + ".pdf";
                String externalFilename = result.get().getSongname().toLowerCase().replaceAll("[^a-z0-9]", "") + ".pdf";
                Path pathname = Paths.get(chordproDirectory, internalFilename);
        
                byte[] contents = Files.readAllBytes(pathname);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_PDF);
                headers.setContentDispositionFormData(externalFilename, externalFilename);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                return new ResponseEntity<>(contents, headers, HttpStatus.OK);
            } catch (IOException e) {
                throw new ResponseStatusException(HttpStatus.NO_CONTENT, "PDF not found");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Song not found");
        }
    }

    @GetMapping(value = "/song/html/{songid}")
    public ResponseEntity<String> getSongAsHtml(@PathVariable("songid") Long songid) {
        logger.info("Requesting song number {} as HTML.", songid);

        Optional<Song> result = songRepository.findById(songid);
        if (result.isPresent()) {
            try {
                String basename = SONGFILENAME_PREFIX + result.get().getSongid();
                String internalFilename = basename + ".html";
                String externalFilename = result.get().getSongname().toLowerCase().replaceAll("[^a-z0-9]", "") + ".html";
                Path pathname = Paths.get(chordproDirectory, internalFilename);
        
                StringBuilder contents = new StringBuilder();
                boolean on = false;
                for (String line : Files.readAllLines(pathname)) {
                    if (line.contains("<body>")) {
                        on = true;
                    } else if (line.contains("</body>")) {
                        on = false;
                    } else if (on) {
                        contents.append(line + "\n");
                    }
                }
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.TEXT_HTML);
                headers.setContentDispositionFormData(externalFilename, externalFilename);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                return new ResponseEntity<>(contents.toString(), headers, HttpStatus.OK);
            } catch (IOException e) {
                throw new ResponseStatusException(HttpStatus.NO_CONTENT, "PDF not found");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Song not found");
        }
    }

    @GetMapping(value = "/songbook/pdf")
    public ResponseEntity<byte[]> getSongbook() {
        logger.info("Fetching songbook");
        
        // Make filelist in right order.
        SortedMap<String, Long> songs = new TreeMap<>();
        for (Song song : songRepository.findAll()) {
            if (song.getChords().contains("Royal Teazer")) {
                songs.put("RT_" + song.getSongname(), song.getSongid());
            } else {
                songs.put("XX_" + song.getSongname(), song.getSongid());
            }
        }
        Path pathname = Paths.get(chordproDirectory, "filelist.txt");
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(pathname.toString()));
            for (Map.Entry<String, Long> entry : songs.entrySet()) {
                String basename = SONGFILENAME_PREFIX + entry.getValue();
                String filename = basename + ".crd";
                writer.write(filename + "\n");
            }
            writer.close();
        } catch (IOException e) {
            logger.warn("Error while writing to filelist: {}", e.getMessage());
            e.printStackTrace();
        }

        // Update songbook
        updateSongbook();

        // Transfer songbook
        try {
            byte[] contents = Files.readAllBytes(Path.of(songbookFilepath));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "royalteazer-songbook.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            return new ResponseEntity<>(contents, headers, HttpStatus.OK);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Songbook not found");
        }
    }    

    @PostMapping("/song/crd")
    public Map<String, String> postSong(
        @RequestBody PostSong postsong,
        @RequestHeader("token") String token) {

		// Look up user and check if key has expired.
		for (User user : userRepository.findAll()) {
			if ((user.getToken() != null) && (user.getToken().equals(token))) {
				if (user.getTokenexpirytime().isBefore(Instant.now())) {
					logger.info("Login of user {} has expired.", user.getUsername());
					throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
				}
				
                logger.info("Posting song {}.", postsong.getSongname());

                if (postsong.getSongname().isEmpty()) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Songname is empty");
                }

                Song newsong = new Song();
                newsong.setSongname(postsong.getSongname());
                newsong.setChords(postsong.getChords());
                newsong.setComments(postsong.getComments());
                newsong.setDate(LocalDate.now());
                if (postsong.getSongid() > 0) {
                    newsong.setSongid(postsong.getSongid());
                }

                Map<String, String> response = new HashMap<>();
                Song posted = songRepository.save(newsong);
                response.put("songid", String.valueOf(posted.getSongid()));
                updateChordproFile(saveChordsToFile(posted));
                return response;
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
    }

    @DeleteMapping(value = "/song/{songid}")
    public Map<String, String> deleteSong(
        @PathVariable("songid") Long songid, 
        @RequestHeader("token") String token) {
        logger.info("Deleting song {}.", songid);

        // Look up user and check if key has expired.
        for (User user : userRepository.findAll()) {
            if ((user.getToken() != null) && (user.getToken().equals(token))) {
                if (user.getTokenexpirytime().isBefore(Instant.now())) {
                    logger.info("Login of user {} has expired.", user.getUsername());
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
                }

                Optional<Song> optional = songRepository.findById(songid);
                if (optional.isPresent()) {
                    Song song = optional.get();
                    
                    // Iterate through all attachments and delete those connected to this song.
                    for (Attachment attachment : attachmentRepository.findAll()) {
                        if (attachment.getSongid().equals(songid)) {
                            Path pathname = Paths.get(attachmentDirectory, attachment.getAttachmentfilename());
                            if (pathname.toFile().delete()) {
                                attachmentRepository.delete(attachment);
                            }
                        }
                    }

                    // Delete songfiles
                    String basename = SONGFILENAME_PREFIX + song.getSongid();
                    Path crdPathname = Paths.get(chordproDirectory, basename + ".crd");
                    Path pdfPathname = Paths.get(chordproDirectory, basename + ".pdf");
                    if (crdPathname.toFile().delete() && pdfPathname.toFile().delete()) {
    
                        // Delete database record
                        songRepository.delete(song);

                        // Recreate songbook
                        updateSongbook();

                        Map<String, String> response = new HashMap<>();
                        response.put("result", "ok");
                        return response;

                    } else {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "File not found");
                    }

                } else {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found");
                }
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
    }

    private String saveChordsToFile(Song song) {

        String title = "{title: " + song.getSongname() + "}\n";
        String basename = SONGFILENAME_PREFIX + song.getSongid();
        String filename = basename + ".crd";
        Path pathname = Paths.get(chordproDirectory, filename);
        BufferedWriter writer;
        logger.info("Writing to file {}", pathname);
        try {
            writer = new BufferedWriter(new FileWriter(pathname.toString()));
            writer.write(title);
            writer.write(song.getChords());
            writer.close();
        } catch (IOException e) {
            logger.warn("Error while writing to file: {}", e.getMessage());
            e.printStackTrace();
        }
        return basename;
    }

    private void updateChordproFile(String basename) {
        
        // Now run script
        logger.debug("Running script {}.", updateScript);
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(updateScript, basename);

        try {

            Process process = processBuilder.start();
            StringBuilder output = new StringBuilder();
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getInputStream())
            );

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                logger.debug("Script exited normally.");
            } else {
                logger.warn("Script had some errors:\n{}", output);
            }

        } catch (IOException e) {
            logger.warn("IO Exception:\n{}", e.getMessage());
        } catch (InterruptedException e) {
            logger.warn("Interrupted exception\n{}", e.getMessage());
        }
    }

    private void updateSongbook() {
        
        // Now run script
        logger.debug("Running script {}.", updateSongbookScript);
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(updateSongbookScript);

        try {

            Process process = processBuilder.start();
            StringBuilder output = new StringBuilder();
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getInputStream())
            );

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                logger.debug("Script exited normally.");
            } else {
                logger.warn("Script had some errors:\n{}", output);
            }

        } catch (IOException e) {
            logger.warn("IO Exception:\n{}", e.getMessage());
        } catch (InterruptedException e) {
            logger.warn("Interrupted exception\n{}", e.getMessage());
        }
    }

}