package nl.royalteazer.rtplanner;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class FrontendController {

	Logger logger = LoggerFactory.getLogger(FrontendController.class);

	// Redirect everything that does not start with api to index.html.
	@RequestMapping(value = { "/", "/{x:[\\w\\-]+}", "/{x:^(?!api$).*$}/**/{y:[\\w\\-]+}" })
    public String getIndex(HttpServletRequest request) {
		logger.info("Requesting index.html.");
        return "/index.html";
    }	

}