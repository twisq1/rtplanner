package nl.royalteazer.rtplanner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalTime;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostAction {
    private String action;
    private List<LocalDate> dates;
    private LocalTime fromtime;
    private LocalTime totime;
    private String comments;
}
