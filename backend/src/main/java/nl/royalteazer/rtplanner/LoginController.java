package nl.royalteazer.rtplanner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class LoginController {

    Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    BuildProperties buildProperties;

    @PostMapping("/login")
    public Map<String, String> login(@RequestBody PostLogin candidate) {

        // Get SHA256 has of password.
        String hash;
        try {
            hash = stringToSha256(candidate.getPassword());
        } catch (NoSuchAlgorithmException e) {
            logger.error("Internal error. Cannot calculate SHA256");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot calculate SHA256.");
        }

        // Check if user has entered valid credentials.
        for (User registratedUser : userRepository.findAll()) {
            if (candidate.getUsername().equals(registratedUser.getUsername())
                    && hash.equals(registratedUser.getPasswordhash())) {

                // Credentials are valid. Create a token and return it.
                Instant expires = Instant.now().plusSeconds(3600L * 24);
                registratedUser.setToken(UUID.randomUUID().toString());
                registratedUser.setTokenexpirytime(expires);
                userRepository.save(registratedUser);
                HashMap<String, String> map = new HashMap<>();
                map.put("userid", registratedUser.getUserid().toString());
                map.put("username", registratedUser.getUsername());
                map.put("realname", registratedUser.getRealname());
                map.put("email", registratedUser.getEmail());
                map.put("token", registratedUser.getToken());
                map.put("role", registratedUser.getRole());
                map.put("tokenexpirytime", registratedUser.getTokenexpirytime().toString());
                logger.info("User {} has logged in succesfully.", registratedUser.getUsername());
                return map;
            }
        }

        // No valid user found. Return 401.
        logger.info("User {} has tried to log in unsuccesfully.", candidate.getUsername());
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid combination of username and password.");
    }


    @GetMapping(value = "/user/{userid}")
    public User getUser(
        @PathVariable("userid") Long userid, 
        @RequestHeader("token") String token) {

        logger.info("Requesting user details of user {}.", userid);

        if (!checkLogin(token)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No valid login");
        }

        Optional<User> result = userRepository.findById(userid);
        if (result.isPresent()) {
            User sanitizedUser = result.get();
            sanitizedUser.setPasswordhash("");
            return sanitizedUser;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
    }


    @PostMapping(value = "/user")
    public Map<String, String> setUser(
        @RequestBody PostUser newuser,
        @RequestHeader("token") String token) {
        logger.info("Setting details of user {}", newuser.getUsername());
        Map<String, String> response = new HashMap<>();

        if (!checkLogin(token)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No valid login");
        }

        // Check if username is unique
        for (User user : userRepository.findAll()) {
            if (!user.getUserid().equals(newuser.getUserid())) {
                if (user.getUsername().equals(newuser.getUsername())) {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, "Usename already exists");
                }
            }
        }

        // Lookup user
        Optional<User> optionalUser = userRepository.findById(newuser.getUserid());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setEmail(newuser.getEmail());
            user.setRealname(newuser.getRealname());
            user.setUsername(newuser.getUsername());
            userRepository.save(user);
            response.put("result", "ok");
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
        return response;
    }

    @PostMapping(value = "/password")
    public Map<String, String> setPassword(
        @RequestBody PostPassword setpassword,
        @RequestHeader("token") String token) {

        logger.info("Setting password of user {}", setpassword.getUserid());
        Map<String, String> response = new HashMap<>();

        if (!checkLogin(token)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No valid login");
        }

        // Lookup user
        Optional<User> optionalUser = userRepository.findById(setpassword.getUserid());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (user.getToken().equals(token)) {
                
                // Check old password.
                try {
                    if (stringToSha256(setpassword.getOldpassword()).equals(user.getPasswordhash())) {

                        // Everything okay, change password.
                        user.setPasswordhash(stringToSha256(setpassword.getNewpassword()));
                        userRepository.save(user);
                        response.put("result", "ok");
                    } else {

                        // Password check failed.
                        throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Wrong old password");
                    }
                } catch (NoSuchAlgorithmException e) {
                    logger.error("Internal error. Cannot calculate SHA256");
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot calculate SHA256.");
                }
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
        return response;
    }

    // Calculate SHA256 hash of plain string.
    private String stringToSha256(String plainString) throws NoSuchAlgorithmException {
        MessageDigest digest;
        digest = MessageDigest.getInstance("SHA-256");
        return bytesToHex(digest.digest(plainString.getBytes(StandardCharsets.UTF_8)));
    }

    // Convert a byte array to a hex string.
    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    // Check if user login is valid.
    private boolean checkLogin(String token) {
        // Look up user and check if key has expired.
		for (User user : userRepository.findAll()) {
			if ((user.getToken() != null) && (user.getToken().equals(token))) {
				if (user.getTokenexpirytime().isBefore(Instant.now())) {
                    logger.info("Login of user {} has expired.", user.getUsername());
                    return false;
                }
                return true;
            }
        }
        logger.info("Invalid token");
        return false;
    }

}