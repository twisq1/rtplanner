package nl.royalteazer.rtplanner;

import org.springframework.data.repository.CrudRepository;

public interface RehearsalRepository extends CrudRepository<Rehearsal, Long> {}
