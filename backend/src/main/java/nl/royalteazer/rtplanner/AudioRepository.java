package nl.royalteazer.rtplanner;

import org.springframework.data.repository.CrudRepository;

public interface AudioRepository extends CrudRepository<Audio, Long> {}
