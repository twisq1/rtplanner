package nl.royalteazer.rtplanner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostUser {

    private Long userid;
    private String username;
    private String realname;
    private String email;
}
