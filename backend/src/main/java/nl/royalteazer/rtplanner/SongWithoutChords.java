package nl.royalteazer.rtplanner;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongWithoutChords {

    private Long songid;
    private String songname;
    private LocalDate date;
    private String comments;
}
