package nl.royalteazer.rtplanner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostClaim {
    private Long songid;
    private List<Long> attachmentids;
}
