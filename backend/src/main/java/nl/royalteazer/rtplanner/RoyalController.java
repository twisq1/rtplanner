package nl.royalteazer.rtplanner;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import java.util.Collection;

import java.time.LocalDate;
import java.time.Instant;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@CrossOrigin
@RestController
@RequestMapping("/api")
public class RoyalController {

	Logger logger = LoggerFactory.getLogger(RoyalController.class);

	@Autowired
	private RehearsalRepository rehearsalRepository;

	@Autowired
	private AvailabilityRepository availabilityRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	BuildProperties buildProperties;
	
	@RequestMapping("/info")
	public Map<String, String> index() {
		logger.info("Info requested");
		Map<String, String> response = new HashMap<>();
		response.put("application", "Royal Teazer Planner");
		response.put("version", buildProperties.getVersion());
		response.put("buildAt", buildProperties.getTime().toString());
		return response;
	}

	// Helper function to add a name to a list and put a comma in between if needed.
	public String addStringToCommalist(String list, String name) {
		if (list == null || list.isEmpty()) {
			return name;
		} else {
			return list + ", " + name;
		}
	}

	@GetMapping("/calendar")
	public Collection<CalendarItem> calendar() {
		logger.info("Calendar requested");
		Map<LocalDate, CalendarItem> calendar = new HashMap<>();

		// Get list of rehearsals.
		for (Rehearsal rehearsal : rehearsalRepository.findAll()) {
			CalendarItem record = new CalendarItem();
			record.setDate(rehearsal.getDate());
			record.setRehearsal(true);
			record.setFromtime(rehearsal.getFromtime());
			record.setTotime(rehearsal.getTotime());
			record.setComments(rehearsal.getComments());
			calendar.put(record.getDate(), record);
		}

		// Fill in availabilities.
		for (Availability availability : availabilityRepository.findAll()) {

			// Look up username.
			String realname = userRepository.findById(availability.getUserid()).orElseThrow().getRealname();

			if (calendar.containsKey(availability.getDate())) {
				updateCalendarRecord(calendar, availability, realname);
			} else {
				createCalendarRecord(calendar, availability, realname);
			}
		}

		return calendar.values();
	}

	private void createCalendarRecord(Map<LocalDate, CalendarItem> calendar, Availability availability, String realname) {
		// Create a new record with only the availability of this user.
		CalendarItem record = new CalendarItem();
		record.setDate(availability.getDate());
		record.setRehearsal(false);
		if (availability.isAvailable()) {
			record.setAvailable(realname);
		} else {
			record.setNotavailable(realname);
		}
		record.setFromtime(availability.getFromtime());
		record.setTotime(availability.getTotime());
		record.setComments(availability.getComments());
		calendar.put(record.getDate(), record);
	}

	private void updateCalendarRecord(Map<LocalDate, CalendarItem> calendar, Availability availability, String realname) {
		// Update availability to existing record.
		CalendarItem record = calendar.get(availability.getDate());
		if (availability.isAvailable()) {
			record.setAvailable(addStringToCommalist(record.getAvailable(), realname));
		} else {
			record.setNotavailable(addStringToCommalist(record.getNotavailable(), realname));
		}

		// Only update fromtime, totime and comments if it is not a rehearsal.
		if (!record.isRehearsal()) {
			if (availability.getFromtime() != null && (record.getFromtime() == null || availability.getFromtime().isAfter(record.getFromtime()))) {
				record.setFromtime(availability.getFromtime());
			}
			if (availability.getTotime() != null && (record.getTotime() == null || availability.getTotime().isBefore(record.getTotime()))) {
				record.setTotime(availability.getTotime());
			}
			record.setComments(addStringToCommalist(record.getComments(), availability.getComments()));
		}
		calendar.put(record.getDate(), record);
	}

	// Clean up the database and delete old events every night at 4AM.
	@Scheduled(cron="0 0 4 * * ?")
	public void deleteOldRecords() {
		int count = 0;
		for (Rehearsal rehearsal : rehearsalRepository.findAll()) {
			if (rehearsal.getDate().isBefore(LocalDate.now())) {
				rehearsalRepository.delete(rehearsal);
				count++;
			}
		}
		for (Availability availability : availabilityRepository.findAll()) {
			if (availability.getDate().isBefore(LocalDate.now())) {
				availabilityRepository.delete(availability);
				count++;
			}
		}
		logger.info("Database cleaned. {} records removed.", count);
	}

	@RequestMapping("/plan")
	public Map<String, String> plan(
		@RequestBody PostAction action,
		@RequestHeader("token") String token) {

		// Look up user and check if key has expired.
		for (User user : userRepository.findAll()) {
			if ((user.getToken() != null) && (user.getToken().equals(token))) {
				if (user.getTokenexpirytime().isBefore(Instant.now())) {
					logger.info("Login of user {} has expired.", user.getUsername());
					throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
				}
				
				// Login is valid. Now take the appropriate action.
				logger.info("User {} requests action {} on {} number of dates.", user.getUsername(), action.getAction(), action.getDates().size());
				Long userid = user.getUserid();
				if (action.getAction().equals("SetAvailable")) {
					executeSetAvailable(action, userid);
				} else if (action.getAction().equals("SetNotAvailable")) {
					executeSetNotAvailable(action, userid);
				} else if (action.getAction().equals("SetRehearse")) {
					executeSetRehearse(action);
				} else if (action.getAction().equals("SetNotRehearse")) {
					executeSetNotRehearse(action);
				} else {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown action.");
				}

				Map<String, String> response = new HashMap<>();
				response.put("username", user.getUsername());
				response.put("status", "OK");	
				return response;
			}
		}
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
	}

	private void executeSetNotRehearse(PostAction action) {
		for (Rehearsal rehearsal : rehearsalRepository.findAll()) {
			if (action.getDates().contains(rehearsal.getDate())) {
				rehearsalRepository.delete(rehearsal);
			}
		}
	}

	private void executeSetRehearse(PostAction action) {
		for (LocalDate datetoset : action.getDates()) {
			boolean done = false;
			for (Rehearsal rehearsal : rehearsalRepository.findAll()) {
				if (datetoset.isEqual(rehearsal.getDate())) {
					rehearsal.setFromtime(action.getFromtime());
					rehearsal.setTotime(action.getTotime());
					rehearsal.setComments(action.getComments());
					rehearsalRepository.save(rehearsal);
					done = true;
				}
			}	
			if (!done) {
				// Make a new record.
				Rehearsal rehearsal = new Rehearsal();
				rehearsal.setDate(datetoset);
				rehearsal.setFromtime(action.getFromtime());
				rehearsal.setTotime(action.getTotime());
				rehearsal.setComments(action.getComments());
				rehearsalRepository.save(rehearsal);
			}
		}
	}

	private void executeSetNotAvailable(PostAction action, Long userid) {
		for (LocalDate datetoset : action.getDates()) {
			boolean done = false;
			for (Availability available : availabilityRepository.findAll()) {
				if (datetoset.isEqual(available.getDate()) && available.getUserid().equals(userid)) {
					available.setAvailable(false);
					available.setComments(action.getComments());
					availabilityRepository.save(available);
					done = true;
				}
			}	
			if (!done) {
				// Create a new record.
				Availability newavailable = new Availability();
				newavailable.setDate(datetoset);
				newavailable.setUserid(userid);
				newavailable.setAvailable(false);
				newavailable.setComments(action.getComments());
				availabilityRepository.save(newavailable);
			}
		}
	}

	private void executeSetAvailable(PostAction action, Long userid) {
		for (LocalDate datetoset : action.getDates()) {
			boolean done = false;
			for (Availability availability : availabilityRepository.findAll()) {
				if (datetoset.isEqual(availability.getDate()) && availability.getUserid().equals(userid)) {
					availability.setAvailable(true);
					availability.setFromtime(action.getFromtime());
					availability.setTotime(action.getTotime());
					availability.setComments(action.getComments());
					availabilityRepository.save(availability);
					done = true;
				}
			}	
			if (!done) {
				// Create a new record.
				Availability newavailable = new Availability();
				newavailable.setDate(datetoset);
				newavailable.setUserid(userid);
				newavailable.setAvailable(true);
				newavailable.setFromtime(action.getFromtime());
				newavailable.setTotime(action.getTotime());
				newavailable.setComments(action.getComments());
				availabilityRepository.save(newavailable);
			}
		}
	}

}