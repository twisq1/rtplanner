package nl.royalteazer.rtplanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RoyalApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoyalApplication.class, args);
	}
	
}
