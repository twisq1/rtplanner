package nl.royalteazer.rtplanner;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AudioItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long audioid;
    private String songname;
    private String audiofilename;
    private LocalDate date;
    private String comments;
}
