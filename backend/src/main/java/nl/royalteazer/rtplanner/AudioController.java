package nl.royalteazer.rtplanner;

import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Optional;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AudioController {

    Logger logger = LoggerFactory.getLogger(AudioController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AudioRepository audioRepository;

    @Autowired
    private SongRepository songRepository;

    @Value("${rtplanner.audio.dir}")
    private String audioDirectory;


    @GetMapping("/audio")
    public Collection<AudioItem> getAllAudio() {
        logger.info("All Audio requested.");

        List<AudioItem> audios = new ArrayList<>();

        // Iterate through all audio files.
        for (Audio audio : audioRepository.findAll()) {
            String songname = "";
            Optional<Song> song = songRepository.findById(audio.getSongid());
            if (song.isPresent()) songname = song.get().getSongname();            
            AudioItem newItem = new AudioItem(audio.getAudioid(), songname, audio.getAudiofilename(), audio.getDate(), audio.getComments());
            audios.add(newItem);
        }

        return audios;
    }


    @GetMapping("/audio/song/{songid}")
    public Collection<Audio> getSongAudio(@PathVariable("songid") Long songid) {
        logger.info("Audio requested of song {}.", songid);

        List<Audio> audios = new ArrayList<>();

        // Iterate through all audio files.
        for (Audio audio : audioRepository.findAll()) {
            if (audio.getSongid().equals(songid)) {
                audios.add(audio);
            }
        }

        return audios;
    }


    @GetMapping(value = "/audio/mp3/{audioid}")
    public ResponseEntity<byte[]> getAudio(@PathVariable("audioid") Long audioid) {
        logger.info("Requesting audio {}.", audioid);

        Optional<Audio> result = audioRepository.findById(audioid);
        if (result.isPresent()) {
            try {
                String filename = result.get().getAudiofilename();
                Path pathname = Paths.get(audioDirectory, filename);
        
                byte[] contents = Files.readAllBytes(pathname);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.parseMediaType("audio/mpeg3"));
                headers.setContentDispositionFormData(filename, filename);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                return new ResponseEntity<>(contents, headers, HttpStatus.OK);
            } catch (IOException e) {
                throw new ResponseStatusException(HttpStatus.NO_CONTENT, "MP3 not found");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Audio not found");
        }
    }

    @DeleteMapping(value = "/audio/{audioid}")
    public Map<String, String> deleteAudio(
        @PathVariable("audioid") Long audioid, 
        @RequestHeader("token") String token) {
        logger.info("Deleting audio {}.", audioid);

        // Look up user and check if key has expired.
        for (User user : userRepository.findAll()) {
            if ((user.getToken() != null) && (user.getToken().equals(token))) {
                if (user.getTokenexpirytime().isBefore(Instant.now())) {
                    logger.info("Login of user {} has expired.", user.getUsername());
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
                }

                Optional<Audio> optional = audioRepository.findById(audioid);
                if (optional.isPresent()) {
                    Audio audio = optional.get();
                    Path pathname = Paths.get(audioDirectory, audio.getAudiofilename());
                    if (pathname.toFile().delete()) {
                        audioRepository.delete(audio);
                        Map<String, String> response = new HashMap<>();
                        response.put("result", "ok");
                        return response;        
                    } else {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "File not found");
                    }

                } else {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found");
                }
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
    }

    @PostMapping("/audio")
    public Map<String, String> postAudio(
            @RequestParam(defaultValue = "0") Long songid,
            @RequestParam("file") MultipartFile file, 
            @RequestParam("comments") String comments,
            @RequestHeader("token") String token) 
    {

        logger.info("Posting audio {}.", file.getOriginalFilename());
 
        // Look up user and check if key has expired.
        for (User user : userRepository.findAll()) {
            if ((user.getToken() != null) && (user.getToken().equals(token))) {
                if (user.getTokenexpirytime().isBefore(Instant.now())) {
                    logger.info("Login of user {} has expired.", user.getUsername());
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
                }

                Path pathname = Paths.get(audioDirectory, file.getOriginalFilename());
                try {
                    file.transferTo(pathname.toFile());
                } catch (IllegalStateException | IOException e) {
                    logger.warn("Error while writing file.");
                    e.printStackTrace();
                    throw new ResponseStatusException(HttpStatus.INSUFFICIENT_STORAGE, "Cannot write file.");
                }

                // Save to database.
                Audio audio = new Audio();
                audio.setSongid(songid);
                audio.setComments(comments);
                audio.setDate(LocalDate.now());
                audio.setAudiofilename(file.getOriginalFilename());
                Audio savedAudio = audioRepository.save(audio);

                Map<String, String> response = new HashMap<>();
                response.put("audioid", Long.toString(savedAudio.getAudioid()));
                return response;
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
    }
}