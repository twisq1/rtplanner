package nl.royalteazer.rtplanner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostPassword {

    private Long userid;
    private String oldpassword;
    private String newpassword;
}
