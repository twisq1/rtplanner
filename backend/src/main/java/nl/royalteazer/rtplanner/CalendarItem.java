package nl.royalteazer.rtplanner;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalTime;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class CalendarItem {
    private LocalDate date;
    private boolean rehearsal;
    private String available;
    private String notavailable;
    private LocalTime fromtime;
    private LocalTime totime;
    private String comments;
}
