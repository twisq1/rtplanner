/* Add two columns to attachment table */
ALTER TABLE `attachment` 
    ADD `date` DATE,
    ADD `comments` VARCHAR(256);
