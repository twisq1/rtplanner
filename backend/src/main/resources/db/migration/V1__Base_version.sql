/* Create user table and insert users. */
CREATE TABLE `user` (
    `userid` INT AUTO_INCREMENT PRIMARY KEY, 
    `username` VARCHAR(64),
    `realname` VARCHAR(64),
    `role` VARCHAR(64),
    `email` VARCHAR(128), 
    `password` VARCHAR(64),
    `token` VARCHAR(48),
    `tokenexpirytime` DATETIME
);

INSERT INTO `user` (`username`, `realname`, `role`, `email`, `password`) VALUES ('paul', 'Paul', 'admin', 'paul@twisq.nl', 'BunkerHok8');
INSERT INTO `user` (`username`, `realname`, `role`, `email`, `password`) VALUES ('leon', 'Leon', 'user', null, 'BunkerHok8');
INSERT INTO `user` (`username`, `realname`, `role`, `email`, `password`) VALUES ('walter', 'Walter', 'user', null, 'BunkerHok8');
INSERT INTO `user` (`username`, `realname`, `role`, `email`, `password`) VALUES ('sjoerd', 'Sjoerd', 'user', null, 'BunkerHok8');
INSERT INTO `user` (`username`, `realname`, `role`, `email`, `password`) VALUES ('edwin', 'Edwin', 'user', null, 'BunkerHok8');

/* Create table for rehearsals. */
CREATE TABLE `rehearsal` (
    `rehearsalid` INT AUTO_INCREMENT PRIMARY KEY,
    `date` DATE,
    `fromtime` TIME, 
    `totime` TIME,
    `comments` VARCHAR(256)
);

/* Create table for availabilities. */
CREATE TABLE `availability` (
    `availabilityid` INT AUTO_INCREMENT PRIMARY KEY,
    `date` DATE,
    `userid` INT,
    `available` BOOLEAN,
    `fromtime` TIME,
    `totime` TIME,
    `comments` VARCHAR(256)
);
