/* Create attachment table. */
CREATE TABLE `attachment` (
    `attachmentid` INT AUTO_INCREMENT PRIMARY KEY,
    `songid` INT, 
    `attachmentfilename` VARCHAR(64)
);
