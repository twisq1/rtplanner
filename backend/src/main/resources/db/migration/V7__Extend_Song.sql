/* Add two columns to song table */
ALTER TABLE `song` 
    ADD `date` DATE,
    ADD `comments` VARCHAR(256);
