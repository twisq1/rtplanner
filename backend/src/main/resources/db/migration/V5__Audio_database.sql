/* Create attachment table. */
CREATE TABLE `audio` (
    `audioid` INT AUTO_INCREMENT PRIMARY KEY,
    `songid` INT, 
    `audiofilename` VARCHAR(64),
    `date` DATE,
    `comments` VARCHAR(256)
);
