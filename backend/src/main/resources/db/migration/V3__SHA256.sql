/* Convert passwords to SHA256 */
UPDATE `user` SET `password`=SHA2(`password`, 256);
ALTER TABLE `user` CHANGE `password` `passwordhash` VARCHAR(64);